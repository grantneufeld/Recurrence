#!/bin/sh

# Runs the more detailed (therefore longer taking) SwiftLint analysis.
# This should at least be run before commits.

xcodebuild -project Recurrence.xcodeproj -scheme Recurrence > xcodebuild.log
swiftlint analyze --compiler-log-path xcodebuild.log
