#  Recurrence: Developer Instructions

## Before Commits

Before doing a commit, run the SwiftLint analyzer, and address any errors or warnings it issues:

    swiftlintanalyze.sh

## Installation & Setup

You’ll need to have SwiftLint installed on your system.
For example, using Homebrew: `brew install swiftlint`
