import SwiftUI

@main
struct RecurrenceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
