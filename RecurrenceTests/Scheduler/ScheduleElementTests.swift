@testable import Recurrence
import XCTest

final class ScheduleElementTests: XCTestCase {
    func test_init_givenEmptyName_setsNameToEmptyString() {
        let sut = ScheduleElement(name: "")
        XCTAssertEqual(sut.name, "")
    }
    func test_init_givenAName_setsNameToThat() {
        let sut = ScheduleElement(name: "A Name")
        XCTAssertEqual(sut.name, "A Name")
    }

    func test_init_timeToNextDefaultsToNil() {
        let sut = ScheduleElement(name: "Nil Time")
        XCTAssertNil(sut.timeToNext)
    }

    func test_init_givenTimeToNextValue_setsTimeToNext() {
        let sut = ScheduleElement(name: "With Time", timeToNext: 123)
        XCTAssertEqual(sut.timeToNext, 123)
    }
}
